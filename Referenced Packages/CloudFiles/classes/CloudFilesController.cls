/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CloudFilesController {
    global CloudFilesController() {

    }
    @RemoteAction
    global static cfr__CloudFile__c attachFile(String name, String url, String objectName, String objectId) {
        return null;
    }
    @RemoteAction
    global static void deleteFile(String id) {

    }
    @RemoteAction
    global static List<cfr__CloudFile__c> getFiles(String objectName, String objectId) {
        return null;
    }
}
