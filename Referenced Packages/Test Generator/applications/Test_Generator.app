<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Use to generate test class.</description>
    <label>Test Generator</label>
    <tab>Test_Generator</tab>
    <tab>Debtor__c</tab>
    <tab>Creditor__c</tab>
    <tab>Sale__c</tab>
    <tab>Purchase__c</tab>
    <tab>Stock__c</tab>
    <tab>Cash__c</tab>
    <tab>Expense__c</tab>
    <tab>items_Google_Drive__x</tab>
</CustomApplication>
